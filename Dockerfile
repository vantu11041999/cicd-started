FROM openjdk:8-jre
EXPOSE 8080
WORKDIR /app
COPY ./target/world-0.0.1-SNAPSHOT.jar .
CMD ["java", "-jar", "world-0.0.1-SNAPSHOT.jar"]